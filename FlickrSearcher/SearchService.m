//
//  SearchService.m
//  FlickrSearcher
//
//  Created by Akif Hossain on 2018-01-11.
//  Copyright © 2018 Akif Hossain. All rights reserved.
//

#import "SearchService.h"

#import <AFNetworking/AFNetworking.h>

#import "Photo.h"

static NSString *const FLICKR_API = @"https://api.flickr.com/services/rest/";

@implementation SearchService {
    AFHTTPSessionManager *_sessionManager;
}

+ (instancetype)sharedInstance {
    static SearchService *instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[self.class alloc] init];
    });
    return instance;
}

- (instancetype)init {
    self = [super init];
    if (self) {
        NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
        _sessionManager = [[AFHTTPSessionManager alloc] initWithSessionConfiguration:configuration];
        _sessionManager.requestSerializer = [AFJSONRequestSerializer serializer];
        
    }
    return self;
}

- (void)loadDataWithQuery:(NSString *)query {
    [self loadDataWithQuery:query page:0 success:nil failure:nil];
}

- (void)loadDataWithQuery:(NSString *)query page:(NSInteger)page success:(void (^)(PhotoResponse *))success failure:(void (^)(NSError *))failure {
    [_sessionManager.operationQueue cancelAllOperations];
    NSDictionary *params = @{
                             @"method":@"flickr.photos.search",
                             @"api_key": @"675894853ae8ec6c242fa4c077bcf4a0",
                             @"text": query,
                             @"extras": @"url_s",
                             @"format": @"json",
                             @"nojsoncallback": @"1",
                             @"page": @(page),
                             };
    
    [_sessionManager GET:FLICKR_API parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSError *error;
        SearchResponse *response = [[SearchResponse alloc] initWithDictionary:(NSDictionary *)responseObject error:&error];
        if (error) {
            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                failure(error);
            }];
        } else {
            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                success(response.photos);
            }];
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            failure(error);
        }];
    }];
}

@end

