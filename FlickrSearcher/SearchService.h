//
//  SearchService.h
//  FlickrSearcher
//
//  Created by Akif Hossain on 2018-01-11.
//  Copyright © 2018 Akif Hossain. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "Photo.h"

@interface SearchService : NSObject
+ (instancetype)sharedInstance;

- (void)loadDataWithQuery:(NSString *)query;
- (void)loadDataWithQuery:(NSString *)query page:(NSInteger)page success:(void (^)(PhotoResponse *response))success failure:(void (^)(NSError *error))failure;
@end
