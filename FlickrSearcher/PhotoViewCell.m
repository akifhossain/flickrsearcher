//
//  PhotoViewCell.m
//  FlickrSearcher
//
//  Created by Akif Hossain on 2018-01-11.
//  Copyright © 2018 Akif Hossain. All rights reserved.
//

#import "PhotoViewCell.h"

#import <AFNetworking/UIImageView+AFNetworking.h>
#import <AFNetworking/AFImageDownloader.h>

@implementation PhotoViewCell {
    UIImageView *_imageView;
}

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        _imageView = [[UIImageView alloc] init];
        _imageView.backgroundColor = [UIColor grayColor];
        _imageView.contentMode = UIViewContentModeScaleAspectFill;
        _imageView.clipsToBounds = YES;
        
        [self addSubview:_imageView];
    }
    return self;
}

- (void)layoutSubviews {
    _imageView.frame = self.bounds;
    
}

- (void)updateCellWithPhoto:(Photo *)photo {
    [_imageView cancelImageDownloadTask];
    _imageView.image = nil;
    
    if (photo.derivedUrl.length > 0) {
        __weak UIImageView *imageView = _imageView;
        
        NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:photo.url_s] cachePolicy:NSURLRequestReturnCacheDataElseLoad timeoutInterval:3600];
        [_imageView setImageWithURLRequest:request placeholderImage:nil success:^(NSURLRequest * _Nonnull request, NSHTTPURLResponse * _Nullable response, UIImage * _Nonnull image) {
            imageView.image = image;
            
        } failure:nil];
    }
}

@end

