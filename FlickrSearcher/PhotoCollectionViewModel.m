//
//  PhotoCollectionViewModel.m
//  FlickrSearcher
//
//  Created by Akif Hossain on 2018-01-11.
//  Copyright © 2018 Akif Hossain. All rights reserved.
//

#import "PhotoCollectionViewModel.h"

#import "PhotoViewCell.h"

@implementation PhotoCollectionViewModel {
    PhotoResponse *_lastResponse;
}

- (void)setQuery:(NSString *)query {
    _query = query;
    _photos = [NSMutableArray array];
    _lastResponse = nil;
}

- (NSInteger)currentPage {
    return _lastResponse.page;
}

- (bool)canLoadMoreData {
    return self.currentPage < _lastResponse.pages;
}

- (void)updateWithResponse:(PhotoResponse *)response {
    _lastResponse = response;
    if (!_photos) {
        _photos = [NSMutableArray array];
    }
    [_photos addObjectsFromArray:response.photo];
}

+ (NSString *)validateQueryWithQuery:(NSString *)query {
    if (!query || query.length == 0 || query.length > 50) {
        return @"Enter a query between 0 to 50 characters";
    }
    return nil;
}

- (NSInteger)collectionView:(nonnull UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.photos.count;
}

- (UICollectionViewCell *)collectionView:(nonnull UICollectionView *)collectionView cellForItemAtIndexPath:(nonnull NSIndexPath *)indexPath {
    PhotoViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"PhotoViewCell" forIndexPath:indexPath];
    
    if (cell) {
        [cell updateCellWithPhoto:self.photos[indexPath.row]];
    }
    
    // we can lazy load aggressively here
    if (indexPath.item == self.photos.count - 10) {
        [self maybeLoadMore];
    }
    
    return cell;
}

- (void)maybeLoadMore {
    if (self.canLoadMoreData) {
        [self.delegate photoCollectionViewModel:self loadDataWithPage:@(self.currentPage + 1)];
    }
}

@end
