//
//  PhotoCollectionViewModel.h
//  FlickrSearcher
//
//  Created by Akif Hossain on 2018-01-11.
//  Copyright © 2018 Akif Hossain. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <UIKit/UIKit.h>
#import "Photo.h"

@class PhotoCollectionViewModel;

@protocol PhotoCollectionViewModelDelegate <NSObject>

- (void)photoCollectionViewModel:(PhotoCollectionViewModel *)viewModel loadDataWithPage:(NSNumber *)page;

@end

@interface PhotoCollectionViewModel : NSObject <UICollectionViewDelegate, UICollectionViewDataSource>

@property (nonatomic) NSString *query;
@property (nonatomic, readonly) NSMutableArray<Photo *> *photos;
@property (nonatomic, readonly) NSInteger currentPage;
@property (nonatomic, readonly) bool canLoadMoreData;
@property (nonatomic, weak) id<PhotoCollectionViewModelDelegate> delegate;

- (void)updateWithResponse:(PhotoResponse *)response;
+ (NSString *)validateQueryWithQuery:(NSString *)query;

@end


