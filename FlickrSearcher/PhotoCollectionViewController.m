//
//  ViewController.m
//  FlickrSearcher
//
//  Created by Akif Hossain on 2018-01-11.
//  Copyright © 2018 Akif Hossain. All rights reserved.
//

#import "PhotoCollectionViewController.h"

#import <UIKit/UIKit.h>

#import "SearchService.h"
#import "PhotoViewCell.h"
#import "PhotoCollectionViewModel.h"

@interface PhotoCollectionViewController () <PhotoCollectionViewModelDelegate, UITextFieldDelegate>

@end

@implementation PhotoCollectionViewController {
    UICollectionView *_collectionView;
    UITextField *_searchTextField;
    PhotoCollectionViewModel *_photoCollectionViewModel;
}

- (void)initializeViews {
    _photoCollectionViewModel = [[PhotoCollectionViewModel alloc] init];
    _photoCollectionViewModel.delegate = self;
    
    _searchTextField = [[UITextField alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 100)];
    _searchTextField.delegate = self;
    _searchTextField.backgroundColor = [UIColor yellowColor];
    _searchTextField.placeholder = @"Search";
    _searchTextField.leftViewMode = UITextFieldViewModeAlways;
    _searchTextField.leftView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"search"]];
    [self.view addSubview:_searchTextField];
    
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    NSInteger numberOfColumns = 3;
    CGFloat itemWidth = (CGRectGetWidth(self.view.frame) - (numberOfColumns - 1)) / numberOfColumns;
    layout.scrollDirection = UICollectionViewScrollDirectionVertical;
    layout.itemSize = CGSizeMake(itemWidth, itemWidth);
    layout.minimumInteritemSpacing = 1;
    layout.minimumLineSpacing = 1;
    
    CGRect frame = CGRectMake(0, _searchTextField.frame.size.height, self.view.frame.size.width, self.view.frame.size.height - _searchTextField.frame.size.height);
    _collectionView = [[UICollectionView alloc] initWithFrame:frame collectionViewLayout:layout];
    [_collectionView registerClass:[PhotoViewCell class] forCellWithReuseIdentifier:@"PhotoViewCell"];
    _collectionView.delegate = _photoCollectionViewModel;
    _collectionView.dataSource = _photoCollectionViewModel;
    _collectionView.backgroundColor = [UIColor whiteColor];
    
    [self.view addSubview:_collectionView];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initializeViews];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    NSString *validation = [PhotoCollectionViewModel validateQueryWithQuery:textField.text];
    if (validation.length > 0) {
        [self displayAlertWithTitle:nil description:validation];
        return NO;
    }
    _photoCollectionViewModel.query = textField.text;
    [self photoCollectionViewModel:_photoCollectionViewModel loadDataWithPage:@(0)];
    return YES;
}

- (void)photoCollectionViewModel:(PhotoCollectionViewModel *)viewModel loadDataWithPage:(NSNumber *)page {
    [[SearchService sharedInstance] loadDataWithQuery:_photoCollectionViewModel.query page:page.intValue success:^(PhotoResponse *response) {
        [self bindResponse:response];
    } failure:^(NSError *error) {
        [self handleError:error];
    }];
}

- (void)bindResponse:(PhotoResponse *)response {
    NSUInteger oldCount = _photoCollectionViewModel.photos.count;
    [_photoCollectionViewModel updateWithResponse:response];
    
    if (_photoCollectionViewModel.currentPage == 1) {
        [_collectionView reloadData];
        [_collectionView setContentOffset:CGPointZero];
    } else {
        [_collectionView performBatchUpdates:^{
            NSMutableArray *arrayWithIndexPaths = [NSMutableArray array];
            for (NSUInteger i = oldCount; i < _photoCollectionViewModel.photos.count; i++) {
                [arrayWithIndexPaths addObject:[NSIndexPath indexPathForRow:i inSection:0]];
            }
            [_collectionView insertItemsAtIndexPaths:arrayWithIndexPaths];
        } completion:nil];
    }
    
    [_searchTextField resignFirstResponder];
}

- (void)handleError:(NSError *)error {
    [self displayAlertWithTitle:@"Failed to load" description:error.description];
}

- (void)displayAlertWithTitle:(NSString *)title description:(NSString *)description {
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:description preferredStyle:UIAlertControllerStyleAlert];
    [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil]];
    [self presentViewController:alertController animated:YES completion:nil];
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    [_searchTextField resignFirstResponder];
}

@end

