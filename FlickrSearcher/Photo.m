//
//  Photo.m
//  FlickrSearcher
//
//  Created by Akif Hossain on 2018-01-11.
//  Copyright © 2018 Akif Hossain. All rights reserved.
//

#import "Photo.h"

@implementation Photo

- (NSString *)derivedUrl {
    if (self.url_s.length > 0) {
        return self.url_s;
    } else {
        return [NSString stringWithFormat:@"https://farm%@.staticflickr.com/%@/%@_%@.jpg", @(self.farm), self.server, self.id, self.secret];
    }
}
@end

@implementation PhotoResponse

@end

@implementation SearchResponse

@end

