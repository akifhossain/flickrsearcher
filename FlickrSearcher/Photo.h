//
//  Photo.h
//  FlickrSearcher
//
//  Created by Akif Hossain on 2018-01-11.
//  Copyright © 2018 Akif Hossain. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <JSONModel/JSONModel.h>


@protocol Photo;

@interface Photo : JSONModel

@property (nonatomic) NSString *id;
@property (nonatomic) NSString *owner;
@property (nonatomic) NSString *secret;
@property (nonatomic) NSString *server;
@property (nonatomic) NSString *title;
@property (nonatomic) NSInteger farm;
@property (nonatomic) bool ispublic;
@property (nonatomic) bool isfriend;
@property (nonatomic) bool isfamily;
@property (nonatomic) NSString<Optional> *url_s; // some data do not provide this

- (NSString *)derivedUrl;

@end


@interface PhotoResponse : JSONModel
@property (nonatomic) NSInteger page;
@property (nonatomic) NSInteger pages;
@property (nonatomic) NSInteger perpage;
@property (nonatomic) NSInteger total;
@property (nonatomic) NSArray<Photo> *photo;

@end


@interface SearchResponse : JSONModel

@property (nonatomic) PhotoResponse *photos;
@property (nonatomic) NSString *stat;

@end

