//
//  PhotoViewCell.h
//  FlickrSearcher
//
//  Created by Akif Hossain on 2018-01-11.
//  Copyright © 2018 Akif Hossain. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "Photo.h"

@interface PhotoViewCell : UICollectionViewCell

- (void)updateCellWithPhoto:(Photo *)photo;

@end

